"""
NeoSense Smart Contract based Licensing
Created by Dean van Dugteren (City of Zion, VDT Network)
hello@dean.press
"""

from boa.interop.Neo.Runtime import CheckWitness, Log
from boa.interop.Neo.Storage import GetContext, Put, Delete, Get
from boa.builtins import concat

KEY = b'contender_'
def Main(operation, args):
    """
    Main definition for the smart contracts

    :param operation: the operation to be performed
    :type operation: str

    :param args: list of arguments.
        args[0] is always sender script hash
        args[1] is always product_id
        args[2] (optional) is always another script hash
    :param type: str

    :return:
        byterarray: The result of the operation
    """
    # Am I who I say I am?
    ctx = GetContext()
    Log(operation)
    Log(args[0])
    if operation == "add":
        Log(operation)
        if len(args) == 2:
            Log(args[0])
            Log(args[1])
            output = args[0] + args[1]
            return output
        return "Sorry, accepts only two arguments to add"
    elif operation == "put":
        Log(operation)
        voting_key = concat(KEY, args[0])
        cur_count = Get(ctx, voting_key)
        Log(cur_count)
        if not cur_count:
            cur_count = 1
        else:
            cur_count = cur_count + 1
        Put(ctx, voting_key, cur_count)
        return True
    elif operation == "get":
        Log(operation)
        voting_key = concat(KEY, args[0])    
        return Get(ctx, voting_key)
    Log("Operation was not add")
    return 2
    # return "Only supported operation is add"